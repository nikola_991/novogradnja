"use strict";

const autoprefixer = require("autoprefixer");
const browsersync = require("browser-sync").create();
const cssnano = require("cssnano");
const del = require("del");
const eslint = require("gulp-eslint");
const uglify = require("gulp-uglify");
const gulp = require("gulp");
const sourcemaps = require("gulp-sourcemaps");
const babel = require("gulp-babel");
const imagemin = require("gulp-imagemin");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");
const sass = require("gulp-sass");


const SCRIPTS_PATH = "js/**/*.js";
const SASS_PATH = "scss/**/*.scss";
const IMAGE_PATH = "img/**/*"
var HTML_PATH = '*.html';


gulp.task('copyHtml', async function () {
  gulp.src(HTML_PATH)
    .pipe(gulp.dest('dist/'));
});


gulp.task("images", async function () {
  console.log("images")
  gulp
    .src(IMAGE_PATH)
    .pipe(
      imagemin([
        imagemin.jpegtran({ progressive: true }),
        imagemin.svgo({
          plugins: [
            {
              removeViewBox: false,
              collapseGroups: true
            }
          ]
        })
      ])
    )
    .pipe(gulp.dest("dist/img"));
});


gulp.task("styles", async function () {
  console.log("starting styles task");
  return gulp
    .src("scss/styles.scss")
    .pipe(
      plumber(function (err) {
        console.log("STYLES:" + err);
        this.emit("end");
      })
    )
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(postcss([]))
    // .pipe(gulp.dest("css/"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(postcss([cssnano(), autoprefixer()]))
    .pipe(sass({ outputStyle: "compressed" }))
    .pipe(gulp.dest("dist/css/"))
    .pipe(browsersync.stream());
});


function scriptsLint() {
  return gulp
    .src(["gulpfile.js"])
    .pipe(plumber())
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
}


gulp.task("scripts", function () {
  console.log("starting scripts task");

  return (
    gulp
      .src("js/**/*.js")
      .pipe(
        plumber(function (err) {
          console.log("SCRIPTS:" + err);
          this.emit("end");
        })
      )
      .pipe(sourcemaps.init())
      .pipe(
        babel({
          presets: ["es2015"]
        })
      )
      // .pipe(gulp.dest("js/"))
      .pipe(uglify())
      .pipe(rename({ suffix: ".min" }))
      .pipe(gulp.dest("dist/js/"))
      .pipe(browsersync.stream())
  );
});


gulp.task("watch", function () {
  console.log("Gulp watch start");
  gulp.watch(HTML_PATH, gulp.series('copyHtml'));
  gulp.watch(SCRIPTS_PATH, gulp.series("scripts"));
  gulp.watch(SASS_PATH, gulp.series("styles"));
  gulp.watch(IMAGE_PATH, gulp.series("images"));

  browsersync.init({
    server: "./dist/"
  });
  gulp.watch("*.html").on('change', browsersync.reload);
})


