console.log('works')

function openNav() {
  document.getElementById("sidenav__phone").style.width = "100%";
  document.getElementById("sidenav__phone").style.height = "100vh";
}

function closeNav() {
  document.getElementById("sidenav__phone").style.width = "0";
  document.getElementById("sidenav__phone").style.height = "100%";

}


document.querySelector("#close").addEventListener("click", function () {
  closeNav();
});

document.querySelector("#open").addEventListener("click", function () {
  openNav();
});


$("a[href^='#']").click(function (e) {
  e.preventDefault();

  const position = $($(this).attr("href")).offset().top;

  $("body, html").animate({
    scrollTop: position
  });
  setTimeout(() => {
    closeNav();
  }, 500);
});